#!/bin/bash

# pull everything
find . -maxdepth 1 -mindepth 1 -type d -print -execdir git --git-dir={}/.git --work-tree="$PWD"/{} pull \;
